import hoomd
import hoomd.md
import hoomd.metal
from hoomd import deprecated
import mbuild, mdtraj, nglview, numpy, freud
import matplotlib.pyplot as plt


import fresnel
import fresnel.tracer
import gsd
import gsd.fl
import gsd.pygsd
import gsd.hoomd
import PIL
import IPython
import io
import math
import sys
import scipy
import scipy.interpolate

def run_randomize_Nb(kT, lattice, size):
    Temperature = kT #Note, with EAM, temperature is measured in electron-Volts
    hoomd.context.initialize("")
    system=hoomd.init.create_lattice(unitcell=hoomd.lattice.bcc(a= lattice,type_name="Nb"),n=size)
    # decrease density and jack up temp to shake everything off the lattice 
    hoomd.md.integrate.mode_standard(dt=0.005)
    nl = hoomd.md.nlist.cell()
    eam = hoomd.metal.pair.eam(file='Nb.eam.alloy', type='FS', nlist=nl)
    all=hoomd.group.all()
    nvt=hoomd.md.integrate.nvt(group=all,kT=Temperature,tau=0.1)
    nvt.randomize_velocities(seed=23) # Randomizing velocities speeds up equilibration.
    hoomd.dump.gsd("Nb_R_"+str(kT)+"_"+str(lattice)+"_"+str(size)+".gsd", period=5000, group=all, overwrite=True) # write out configurations periodically. Can skip if doing all analysis in real-time
    hoomd.run(50000)
    print("Nb_R_"+str(kT)+"_"+str(lattice)+"_"+str(size)+".gsd")
    init_data = hoomd.data.gsd_snapshot("Nb_R_"+str(kT)+"_"+str(lattice)+"_"+str(size)+".gsd", 9)
    return init_data

def run_preinit_Nb(kT, runtime=50000, period= 2500, lattice=6,size=8):
    #the number of atoms in the sim is [(size)^3]*2
    Temperature = kT #Note, with EAM, temperature is measured in electron-Volts
    init_data=run_randomize_Nb(kT=2, lattice=lattice, size =size)
    hoomd.context.initialize("")
    system = hoomd.init.read_snapshot(init_data)
    # initializes from snap shot created by run randomize function
    deprecated.dump.xml(filename="Nb-init.hoomdxml",group=hoomd.group.all(),vis=True)
    hoomd.md.integrate.mode_standard(dt=0.005)
    nl = hoomd.md.nlist.cell()
    eam = hoomd.metal.pair.eam(file='Nb.eam.alloy', type='FS', nlist=nl)
    all=hoomd.group.all()
    nvt=hoomd.md.integrate.nvt(group=all,kT=Temperature,tau=0.1)
    nvt.randomize_velocities(seed=23) # Randomizing velocities speeds up equilibration.
    hoomd.dump.gsd("Nb_post"+"_"+str(runtime)+"_"+str(size)+"_"+str(lattice)+"_"+str(kT)+".gsd", period, group=all, overwrite=True) # write out configurations periodically. Can skip if doing all analysis in real-time
    hoomd.analyze.log(filename="Nb_post"+"_"+str(runtime)+"_"+str(size)+"_"+str(lattice)+"_"+str(kT)+".log",quantities=['potential_energy','temperature','pressure','kinetic_energy'],period=500,overwrite=True)
    hoomd.run(runtime)
    deprecated.dump.xml(filename="Nb_post_final.hoomdxml",group=hoomd.group.all(),vis=True)
    print("Nb_post"+"_"+str(runtime)+"_"+str(size)+"_"+str(lattice)+"_"+str(kT)+".gsd")


    
temp = float(sys.argv[1])   
atoms = int(sys.argv[2])
time = int(sys.argv[3])
a=float(sys.argv[4]) 
period = int(sys.argv[5])

run_preinit_Nb(kT=temp, runtime=time, period= period, lattice=a,size=atoms)